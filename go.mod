module gitlab.com/samcarpentier/packer-builder-openstack-rebuild

go 1.14

require (
	github.com/gophercloud/gophercloud v0.12.0
	github.com/gophercloud/utils v0.0.0-20200508015959-b0167b94122c
	github.com/hashicorp/go-cleanhttp v0.5.1
	github.com/hashicorp/hcl/v2 v2.6.0
	github.com/hashicorp/packer v1.6.0
	github.com/mitchellh/mapstructure v1.3.2
	github.com/zclconf/go-cty v1.4.0
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
