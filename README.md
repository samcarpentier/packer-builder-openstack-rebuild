# packer-builder-openstack-rebuild

This is a custom Packer plugin for OpenStack. The builder rebuilds an existing server from a given image and then applies the provisioning steps to it.

## Flow

**NOTE**: Only the minimal operations were implemented to fill my needs. There is much more that could've been done!

The flow of the `openstack-rebuild` provider is very similar to the standard OpenStack builder.

1. Find the target server by name or ID
2. Start the target server
3. Rebuild the target server with the base image
4. Await SSH connectivity and run provisioners/post-processors
5. Shutdown target server
6. Takes a snapshot of the target server and saves it as the output image
7. Leaves the target server un a shutdown state, ready for the next build

## Local development

1. Run `make generate` to generate HCL2 config specs
3. Run `make dist-local` to build and copy the go binary in `~/.packer.d/plugins/`
