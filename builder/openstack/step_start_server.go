package openstack

import (
	"context"
	"fmt"
	"log"

	"github.com/gophercloud/gophercloud"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/extensions/startstop"
	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"github.com/hashicorp/packer/helper/multistep"
	"github.com/hashicorp/packer/packer"
)

type StepStartServer struct{}

func (s *StepStartServer) Run(ctx context.Context, state multistep.StateBag) multistep.StepAction {
	ui := state.Get("ui").(packer.Ui)
	config := state.Get("config").(*Config)
	server := state.Get("server").(*servers.Server)

	// We need the v2 compute client
	client, err := config.computeV2Client()
	if err != nil {
		err = fmt.Errorf("Error initializing compute client: %s", err)
		state.Put("error", err)
		return multistep.ActionHalt
	}

	ui.Say(fmt.Sprintf("Starting server: %s ...", server.ID))

	if err := startstop.Start(client, server.ID).ExtractErr(); err != nil {
		if _, ok := err.(gophercloud.ErrDefault409); ok {
			// The server might have already been shut down by Windows Sysprep
			log.Printf("[WARN] 409 on starting an already running server, continuing")
			return multistep.ActionContinue
		}

		err = fmt.Errorf("Error starting server: %s", err)
		state.Put("error", err)
		return multistep.ActionHalt
	}

	ui.Say(fmt.Sprintf("Waiting for server to start: %s ...", server.ID))
	stateChange := StateChangeConf{
		Pending:   []string{"SHUTOFF", "STOPPED"},
		Target:    []string{"ACTIVE"},
		Refresh:   ServerStateRefreshFunc(client, server),
		StepState: state,
	}
	if _, err := WaitForState(&stateChange); err != nil {
		err := fmt.Errorf("Error waiting for server (%s) to start: %s", server.ID, err)
		state.Put("error", err)
		ui.Error(err.Error())
		return multistep.ActionHalt
	}
	return multistep.ActionContinue
}

func (s *StepStartServer) Cleanup(state multistep.StateBag) {
	config := state.Get("config").(*Config)
	server := state.Get("server").(*servers.Server)
	ui := state.Get("ui").(packer.Ui)

	// We need the v2 compute client
	client, err := config.computeV2Client()
	if err != nil {
		err = fmt.Errorf("Error initializing compute client: %s", err)
		state.Put("error", err)
		return
	}

	ui.Say(fmt.Sprintf("Stopping server: %s ...", server.ID))
	if err := startstop.Stop(client, server.ID).ExtractErr(); err != nil {
		if _, ok := err.(gophercloud.ErrDefault409); ok {
			// The server might have already been shut down by Windows Sysprep
			log.Printf("[WARN] 409 on stopping an already stopped server, continuing")
			return
		}

		err = fmt.Errorf("Error stopping server: %s", err)
		state.Put("error", err)
		return
	}

	ui.Say(fmt.Sprintf("Waiting for server to stop: %s ...", server.ID))
	stateChange := StateChangeConf{
		Pending:   []string{"ACTIVE"},
		Target:    []string{"SHUTOFF", "STOPPED"},
		Refresh:   ServerStateRefreshFunc(client, server),
		StepState: state,
	}

	WaitForState(&stateChange)
}
