package openstack

import (
	"errors"
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/imageservice/v2/images"
	"github.com/hashicorp/packer/common/uuid"
	"github.com/hashicorp/packer/helper/communicator"
	"github.com/hashicorp/packer/template/interpolate"
)

// RunConfig contains configuration for running an instance from a source image
// and details on how to access that launched image.
type RunConfig struct {
	Comm communicator.Config `mapstructure:",squash"`
	// The type of interface to connect via SSH. Values useful for Rackspace
	// are "public" or "private", and the default behavior is to connect via
	// whichever is returned first from the OpenStack API.
	SSHInterface string `mapstructure:"ssh_interface" required:"false"`
	// The IP version to use for SSH connections, valid values are `4` and `6`.
	// Useful on dual stacked instances where the default behavior is to
	// connect via whichever IP address is returned first from the OpenStack
	// API.
	SSHIPVersion string `mapstructure:"ssh_ip_version" required:"false"`
	// The ID or full URL to the base image to use. This is the image that will
	// be used to launch a new server and provision it. Unless you specify
	// completely custom SSH settings, the source image must have cloud-init
	// installed so that the keypair gets assigned properly.
	SourceImage string `mapstructure:"source_image" required:"true"`
	// The name of the base image to use. This is an alternative way of
	// providing source_image and only either of them can be specified.
	SourceImageName string `mapstructure:"source_image_name" required:"true"`
	// Filters used to populate filter options. Example:
	//
	// ```json
	//{
	//     "source_image_filter": {
	//         "filters": {
	//             "name": "ubuntu-16.04",
	//             "visibility": "protected",
	//             "owner": "d1a588cf4b0743344508dc145649372d1",
	//             "tags": ["prod", "ready"],
	//             "properties": {
	//                 "os_distro": "ubuntu"
	//             }
	//         },
	//         "most_recent": true
	//     }
	// }
	// ```
	//
	// This selects the most recent production Ubuntu 16.04 shared to you by
	// the given owner. NOTE: This will fail unless *exactly* one image is
	// returned, or `most_recent` is set to true. In the example of multiple
	// returned images, `most_recent` will cause this to succeed by selecting
	// the newest image of the returned images.
	//
	// -   `filters` (map of strings) - filters used to select a
	// `source_image`.
	//     NOTE: This will fail unless *exactly* one image is returned, or
	//     `most_recent` is set to true. Of the filters described in
	//     [ImageService](https://developer.openstack.org/api-ref/image/v2/), the
	//     following are valid:
	//
	//     -   name (string)
	//     -   owner (string)
	//     -   tags (array of strings)
	//     -   visibility (string)
	//     -   properties (map of strings to strings) (fields that can be set
	//         with `openstack image set --property key=value`)
	//
	// -   `most_recent` (boolean) - Selects the newest created image when
	// true.
	//     This is most useful for selecting a daily distro build.
	//
	// You may set use this in place of `source_image` If `source_image_filter`
	// is provided alongside `source_image`, the `source_image` will override
	// the filter. The filter will not be used in this case.
	SourceImageFilters ImageFilter `mapstructure:"source_image_filter" required:"true"`
	// The availability zone to launch the server in. If this isn't specified,
	// the default enforced by your OpenStack cluster will be used. This may be
	// required for some OpenStack clusters.
	AvailabilityZone string `mapstructure:"availability_zone" required:"false"`

	// Not really used, but here for BC
	OpenstackProvider string `mapstructure:"openstack_provider"`

	sourceImageOpts images.ListOpts

	// The server ID to start and rebuild with the target image
	RebuildTargetServerID string `mapstructure:"rebuild_target_server_id" required:"true"`
	// The server name to start and rebuild with the target image
	RebuildTargetServerName string `mapstructure:"rebuild_target_server_name" required:"true"`
}

type ImageFilter struct {
	// filters used to select a source_image. NOTE: This will fail unless
	// exactly one image is returned, or most_recent is set to true. Of the
	// filters described in ImageService, the following are valid:
	Filters ImageFilterOptions `mapstructure:"filters" required:"false"`
	// Selects the newest created image when true. This is most useful for
	// selecting a daily distro build.
	MostRecent bool `mapstructure:"most_recent" required:"false"`
}

type ImageFilterOptions struct {
	Name       string            `mapstructure:"name"`
	Owner      string            `mapstructure:"owner"`
	Tags       []string          `mapstructure:"tags"`
	Visibility string            `mapstructure:"visibility"`
	Properties map[string]string `mapstructure:"properties"`
}

func (f *ImageFilterOptions) Empty() bool {
	return f.Name == "" && f.Owner == "" && len(f.Tags) == 0 && f.Visibility == "" && len(f.Properties) == 0
}

func (f *ImageFilterOptions) Build() (*images.ListOpts, error) {
	opts := images.ListOpts{}
	// Set defaults for status, member_status, and sort
	opts.Status = images.ImageStatusActive
	opts.MemberStatus = images.ImageMemberStatusAccepted
	opts.Sort = "created_at:desc"

	var err error

	if f.Name != "" {
		opts.Name = f.Name
	}
	if f.Owner != "" {
		opts.Owner = f.Owner
	}
	if len(f.Tags) > 0 {
		opts.Tags = f.Tags
	}
	if f.Visibility != "" {
		v, err := getImageVisibility(f.Visibility)
		if err == nil {
			opts.Visibility = *v
		}
	}

	return &opts, err
}

func (c *RunConfig) Prepare(ctx *interpolate.Context) []error {
	// If we are not given an explicit ssh_keypair_name or
	// ssh_private_key_file, then create a temporary one, but only if the
	// temporary_key_pair_name has not been provided and we are not using
	// ssh_password.
	if c.Comm.SSHKeyPairName == "" && c.Comm.SSHTemporaryKeyPairName == "" &&
		c.Comm.SSHPrivateKeyFile == "" && c.Comm.SSHPassword == "" {

		c.Comm.SSHTemporaryKeyPairName = fmt.Sprintf("packer_%s", uuid.TimeOrderedUUID())
	}

	// Validation
	errs := c.Comm.Prepare(ctx)

	if c.Comm.SSHKeyPairName != "" {
		if c.Comm.Type == "winrm" && c.Comm.WinRMPassword == "" && c.Comm.SSHPrivateKeyFile == "" {
			errs = append(errs, errors.New("A ssh_private_key_file must be provided to retrieve the winrm password when using ssh_keypair_name."))
		} else if c.Comm.SSHPrivateKeyFile == "" && !c.Comm.SSHAgentAuth {
			errs = append(errs, errors.New("A ssh_private_key_file must be provided or ssh_agent_auth enabled when ssh_keypair_name is specified."))
		}
	}

	if c.SourceImage == "" && c.SourceImageName == "" && c.SourceImageFilters.Filters.Empty() {
		errs = append(errs, errors.New("Either a source_image, a source_image_name, or source_image_filter must be specified"))
	} else if len(c.SourceImage) > 0 && len(c.SourceImageName) > 0 {
		errs = append(errs, errors.New("Only a source_image or a source_image_name can be specified, not both."))
	}

	if c.SSHIPVersion != "" && c.SSHIPVersion != "4" && c.SSHIPVersion != "6" {
		errs = append(errs, errors.New("SSH IP version must be either 4 or 6"))
	}

	// if neither ID or image name is provided outside the filter, build the
	// filter
	if len(c.SourceImage) == 0 && len(c.SourceImageName) == 0 {

		listOpts, filterErr := c.SourceImageFilters.Filters.Build()

		if filterErr != nil {
			errs = append(errs, filterErr)
		}
		c.sourceImageOpts = *listOpts
	}

	if c.RebuildTargetServerID == "" && c.RebuildTargetServerName == "" {
		errs = append(errs, fmt.Errorf("One of rebuild_target_server_id or rebuild_target_server_name must be specified"))
	}

	return errs
}

// Retrieve the specific ImageVisibility using the exported const from images
func getImageVisibility(visibility string) (*images.ImageVisibility, error) {
	visibilities := [...]images.ImageVisibility{
		images.ImageVisibilityPublic,
		images.ImageVisibilityPrivate,
		images.ImageVisibilityCommunity,
		images.ImageVisibilityShared,
	}

	for _, v := range visibilities {
		if string(v) == visibility {
			return &v, nil
		}
	}

	return nil, fmt.Errorf("Not a valid visibility: %s", visibility)
}
