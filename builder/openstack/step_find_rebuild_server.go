package openstack

import (
	"context"
	"fmt"

	"github.com/gophercloud/gophercloud/openstack/compute/v2/servers"
	"github.com/gophercloud/gophercloud/pagination"
	"github.com/hashicorp/packer/helper/multistep"
	"github.com/hashicorp/packer/packer"
)

type StepFindRebuildServer struct {
	RebuildTargetServerID   string
	RebuildTargetServerName string
	server                  *servers.Server
}

func (s *StepFindRebuildServer) Run(ctx context.Context, state multistep.StateBag) multistep.StepAction {
	config := state.Get("config").(*Config)
	ui := state.Get("ui").(packer.Ui)

	// We need the v2 compute client
	computeClient, err := config.computeV2Client()
	if err != nil {
		err = fmt.Errorf("Error initializing compute client: %s", err)
		state.Put("error", err)
		return multistep.ActionHalt
	}

	ui.Say("Searching for rebuild server...")
	var rebuildServer *servers.Server = nil

	if s.RebuildTargetServerID != "" {
		ui.Say(fmt.Sprintf("Retrieving information for server with ID: %s", s.RebuildTargetServerID))
		server, _ := servers.Get(computeClient, s.RebuildTargetServerID).Extract()

		rebuildServer = server
		if rebuildServer == nil {
			err := fmt.Errorf("Server with ID %s not found in current project", s.RebuildTargetServerID)
			state.Put("error", err)
			ui.Error(err.Error())
			return multistep.ActionHalt
		}
	} else {
		listOpts := servers.ListOpts{
			Name: s.RebuildTargetServerName,
		}
		pager := servers.List(computeClient, listOpts)

		err := pager.EachPage(func(page pagination.Page) (bool, error) {
			serverList, err := servers.ExtractServers(page)
			if err != nil {
				return false, err
			}

			rebuildServer = &serverList[0]
			return true, nil
		})

		if err != nil {
			err := fmt.Errorf("Error finding server with name %s: %s", s.RebuildTargetServerName, err)
			state.Put("error", err)
			ui.Error(err.Error())
			return multistep.ActionHalt
		}

		if rebuildServer == nil {
			err := fmt.Errorf("Server with name %s not found in current project", s.RebuildTargetServerName)
			state.Put("error", err)
			ui.Error(err.Error())
			return multistep.ActionHalt
		}
	}

	ui.Say("Found rebuild server")
	s.server = rebuildServer
	state.Put("server", s.server)
	// instance_id is the generic term used so that users can have access to the
	// instance id inside of the provisioners, used in step_provision.
	state.Put("instance_id", s.server.ID)

	return multistep.ActionContinue
}

func (s *StepFindRebuildServer) Cleanup(state multistep.StateBag) {}
