package main

import (
	"github.com/hashicorp/packer/packer/plugin"
	"gitlab.com/samcarpentier/packer-builder-openstack-rebuild/builder/openstack"
)

func main() {
	server, err := plugin.Server()
	if err != nil {
		panic(err)
	}

	server.RegisterBuilder(new(openstack.Builder))
	server.Serve()
}
