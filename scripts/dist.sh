#!/bin/bash -e

cd $(cd `dirname "$0"`; cd ..; pwd)

VERSION=$1
if [ -z $VERSION ]; then
	echo "Usage: $0 VERSION"
	exit 1
fi

TARGETS="darwin_386 darwin_amd64 linux_386 linux_amd64 linux_arm windows_386 windows_amd64"

for target in $TARGETS; do
	echo "Building ${target}"
	t=(${target//_/ })

	export GOOS=${t[0]}
	export GOARCH=${t[1]}
	export NAME=packer-builder-openstack-rebuild

	if [ $GOOS == "windows" ]; then
		NAME=${NAME}.exe
	fi

	go build -ldflags "-X main.buildVersion=$VERSION" -o bin/${NAME}

	pushd bin > /dev/null
	tar -cvJf ${NAME}_${VERSION}_${GOOS}_${GOARCH}.tar.xz ${NAME}
	rm ${NAME}
	popd > /dev/null
done
